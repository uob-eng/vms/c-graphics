# COMS30020 Vagrant File
> Vagrant File for use on COMS30020


### Linux
### Prerequisites
* VirtualBox https://www.virtualbox.org/wiki/Downloads
* Vagrant    https://www.vagrantup.com/downloads

### Mac
* VirtualBox https://www.virtualbox.org/wiki/Downloads
* Vagrant   https://www.vagrantup.com/downloads
* XQuartz   https://www.xquartz.org

### Windows
* VirtualBox https://www.virtualbox.org/wiki/Downloads
* Vagrant   https://www.vagrantup.com/downloads
* PuTTy and PuttyGen http://www.chiark.greenend.org.uk/~sgtatham/putty/
* Xming http://sourceforge.net/project/downloading.php?group_id=156984&filename=Xming-6-9-0-31-setup.exe

